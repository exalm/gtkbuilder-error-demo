#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define DEMO_TYPE_SOMETHING (demo_something_get_type())

G_DECLARE_FINAL_TYPE (DemoSomething, demo_something, DEMO, SOMETHING, GtkWidget)

G_END_DECLS
